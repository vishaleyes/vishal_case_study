<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $table = 'cart';

    protected $fillable = [
        'session_id',
        'user_id',
        'product_id',
        'qty'
    ];

    public static $rules = [
        'session_id' => 'required',
        'user_id' => 'required',
        'product_id' => 'required',
        'qty' => 'required|numeric|max:100'
    ];

    public static $updateRules = [
        'session_id' => 'required',
        'user_id' => 'required',
        'product_id' => 'required',
        'qty' => 'required|numeric|max:100'
    ];
}
