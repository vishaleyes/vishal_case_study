<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'user',
        'price',
        'category',
        'description',
        'avatar'


    ];


    public static $rules = [
        'name' => 'required',
        'price' => 'required',
        'category' => 'required',
        //'sub_categories' => 'required',
        'avatar' => 'required|image|mimes:png,jpg,jpeg|max:102400'
    ];

    public static $updateRules = [
        'name' => 'required',
        'price' => 'required',
        'category' => 'required',
        //'sub_categories' => 'required',
        'avatar' => 'required|image|mimes:png,jpg,jpeg|max:102400'
    ];

}
