<?php

namespace App\Http\Controllers\API;

use App\Models\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class CartController extends Controller
{
    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $limit = request('limit') ? request('limit') : env('LIMIT_10');
        $user   = Auth::guard('api')->user();
        if(auth('api')->check()){
            $cartItems = Cart::where('user_id', $user->id)->where('session_id', $request->session_id)->simplePaginate($limit);
        } else {
            $cartItems = Cart::where('session_id', $request->session_id)->simplePaginate($limit);
        }// print "<pre>"; print_r($cartItems); die;
        return response()->json(['status' => $this->successStatus, 'success' => $cartItems], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        try {
            //code...
            $rules = Cart::$rules;
            //validate given data
            $validator = \Validator::make($request->all(), [
                'session_id' => $rules['session_id'],
                'product_id' => $rules['product_id'],
                'qty' => $rules['qty']
                
                ]);

            // if validator fails
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->all(), 'status' => 'error'], 400);
            } else {
                $user   = Auth::guard('api')->user();
                
                $session_id = $request->get('session_id');
                $user_id = $user->id ? $user->id : 0;
                $product_id = $request->get('product_id');
                $qty = $request->get('qty');
                
                //$avatar = $request->get('avatar');

                $cart = new Cart();
                $cart->session_id = $session_id;
                $cart->user_id = $user_id;
                $cart->product_id = $product_id;
                $cart->qty = $qty;
                 
                $cart->save();
                return response()->json(['status' => $this->successStatus, 'success' => $cart], $this->successStatus);
            }
        } catch (\Exception $e) {
            //throw $th;
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        try {
            //code...
            $rules = Cart::$rules;
            //validate given data
            $validator = \Validator::make($request->all(), [
                'session_id' => $rules['session_id'],
                'product_id' => $rules['product_id'],
                'qty' => $rules['qty'],
                'updated_at' => Carbon::now()->toDateTimeString()
                ]);

            // if validator fails
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->all(), 'status' => 'error'], 400);
            } else {
                $user = Auth::guard('api')->user();
                if(!$user){
                    $user_id = 0;
                } else {
                    $user_id = $user->id;
                }
                $session_id = $request->get('session_id');
                $product_id = $request->get('product_id');
                $qty = $request->get('qty');
                
                //$avatar = $request->get('avatar');
                $cart = Cart::find($request->id);
                $cart->session_id = $session_id;
                $cart->user_id = $user_id;
                $cart->product_id = $product_id;
                $cart->qty = $qty;
                 
                $cart->update();
                return response()->json(['status' => $this->successStatus, 'success' => $cart], $this->successStatus);
            }
        } catch (\Exception $e) {
            //throw $th;
            print "<pre>"; print_r($e);die;
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        try {
            //code...
            if($request->id && $request->id != ""){
                $cartItem = Cart::findorFail($request->id); 
                if($cartItem){
                    $cartItem->delete();
                }
                return response()->json(['status' => $this->successStatus, 'success' => 'Deleted Successfully'], $this->successStatus);
              } else {
                return response()->json(['status' => 200, 'error' => "ID parameter is required"], 200);
              }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'error' => $e->getMessage()], 500);
        }
    }
}
