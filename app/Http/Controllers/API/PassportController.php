<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class PassportController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        try {
            $loginData = $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);


            if (!auth()->attempt($loginData)) {
                return response(['message' => 'Invalid Credentials']);
            }

            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

                $user = Auth::user();
                $success = $user;
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                return response()->json(['status' => $this->successStatus, 'success' => $success], $this->successStatus);
            }
            else{
                return response()->json(['status' => 401, 'error'=>'Invalid Credentials'], 401);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 422, 'error' => $e->getMessage()], 422);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);
            }

            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;

            return response()->json(['success'=>$success], $this->successStatus);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'error' => $e->getMessage()], 500);
        }

    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function getDetails()
    {
        try {
            $user = Auth::guard('api')->user();
            return response()->json(['success' => $user], $this->successStatus);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'error' => $e->getMessage()], 500);
        }
    }
}

