<?php

namespace App\Http\Controllers\API;

use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Exception;

class ProductsController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $limit = request('limit') ? request('limit') : env('LIMIT_10');
        $products = Products::paginate($limit);
        return response()->json(['status' => $this->successStatus, 'success' => $products], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        try {
            //code...
            $rules = Products::$rules;
            //validate given data
            $validator = \Validator::make($request->all(), [
                'name' => $rules['name'],
                'category' => $rules['category'],
                'price' => $rules['price'],
                'avatar' => $rules['avatar']
                
                ]);

            // if validator fails
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->all(), 'status' => 'error'], 400);
            } else {
                $user   = Auth::guard('api')->user();
                $name = $request->get('name');
                $category = $request->get('category');
                $price = $request->get('price');
                $description = $request->get('description') ? $request->get('description') : "";
                $avatar = $request->file('avatar');

                $product = new Products();
                $product->name = $name;
                $product->user = $user->id;
                $product->category = $category;
                $product->description = $description;
                $product->price = $price;
               
                $name = rand(11111, 99999) . '.' . $avatar->getClientOriginalExtension();
                $request->file('avatar')->move("fotoupload", $name);
                $product->avatar = $name;
                
                $product->save();
                return response()->json(['status' => $this->successStatus, 'success' => $product], $this->successStatus);
            }
        } catch (Exception $e) {
            //throw $th;
            echo $e->getMessage();
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        try {
            //code...
            if(request('id')){
                $product =  Products::findorFail(request('id')); 
                return response()->json(['status' => $this->successStatus, 'success' => $product], $this->successStatus);
              } else {
                  return response()->json(['status' => 200, 'error' => "ID parameter is required"], 200);
              }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        try {
            //code...
            if(request('id') && request('id') != ""){
                $product = Products::findorFail(request('id')); 
                if($product){
                    $product->delete();
                }
                return response()->json(['status' => $this->successStatus, 'success' => "Deleted"], $this->successStatus);
              } else {
                  return response()->json(['status' => 200, 'error' => "ID parameter is required"], 200);
              }
        } catch (Exception $e) {
            return response()->json(['status' => 500, 'error' => $e->getMessage()], 500);
        }
    }
}
