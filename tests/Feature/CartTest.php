<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Cart;
use App\Models\User;
use App\Models\Products;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class CartTest extends TestCase
{
    /**
     * Save item into cart test case.
     *
     * @return void
     */
    public function testCartItemCreatedSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        
        Storage::fake('avatars');
        $file = UploadedFile::fake()->image('avatar.jpg');

        $product = Products::create([
            "name" => rand(),
            "user" => $user->id,
            "price" => 250,
            "category" => 1,
            "description" => "Video-sharing platform",
            "avatar" => $file,
        ]);

        $cartData = [
            "session_id" => Str::random(30),
            "user_id" => $user->id,
            "product_id" => $product->id,
            "qty" => 1

        ];
        

        $this->json('POST', 'api/cart', $cartData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "success" => [
                    "session_id",
                    "user_id",
                    "product_id",
                    "qty"
                ],
                "status"
            ]);
    }

    /**
     * Get Cart Item List Test Case.
     *
     * @return void
     */
    public function testGetCartItemsSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $this->json('GET', 'api/cart', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "success" => [
                    "current_page"    
                ]
                
            ]);
    }

    /**
     * Get Product By Id Test Case.
     *
     * @return void
     */
    public function testUpdateCartItemSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        Storage::fake('avatars');
        $file = UploadedFile::fake()->image('avatar.jpg');

        $product = Products::create([
            "name" => rand(),
            "user" => $user->id,
            "price" => 250,
            "category" => 1,
            "description" => "Video-sharing platform",
            "avatar" => $file,
        ]);

        $cart = Cart::create([
            "session_id" => Str::random(30),
            "user_id" => $user->id,
            "product_id" => $product->id,
            "qty" => 1
        ]);

        $cartData = [
            "session_id" => $cart->session_id,
            "user_id" => $cart->user_id,
            "product_id" => $cart->product_id,
            "qty" => 2

        ];

        
        $this->json('PUT', 'api/cart/'.$cart->id, $cartData,  ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "status" => 200
            ]);
    }

    /**
     * A feature test to delete cart item 
     *
     * @return void
     */
    public function testDeleteCartItemSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
 
        Storage::fake('avatars');
        $file = UploadedFile::fake()->image('avatar.jpg');

        $product = Products::create([
            "name" => rand(),
            "user" => $user->id,
            "price" => 250,
            "category" => 1,
            "description" => "Video-sharing platform",
            "avatar" => $file,
        ]);

        $cart = Cart::create([
            "session_id" => Str::random(30),
            "user_id" => $user->id,
            "product_id" => $product->id,
            "qty" => 1
        ]);

        $this->json('DELETE', 'api/cart/' . $cart->id)
        ->assertStatus(200)
        ->assertJson([
            'status' => '200',
            'success' => 'Deleted Successfully',
        ]);
       
    }
}
