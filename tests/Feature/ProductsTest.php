<?php

namespace Tests\Feature;

use App\Models\Products;
use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


class ProductsTest extends TestCase
{

    /**
     * A product create test case.
     *
     * @return void
     */
    public function testProductCreatedSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        Storage::fake('avatars');

        $file = UploadedFile::fake()->image('avatar.jpg');

        $productData = [
            "name" => "Susan Wojcicki",
            //"user" => $user->id,
            "price" => 250,
            "category" => 1,
            "description" => "Video-sharing platform",
            "avatar" => $file
        ];


        $response = $this->json('POST', 'api/products', $productData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "success" => [
                    "name",
                    "category",
                    "price",
                    "user"
                ],
                "status"
            ]);

    }

    /**
     * Get Product List Test Case.
     *
     * @return void
     */
    public function testGetProductsSuccessfully()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');
        $this->json('GET', 'api/products?limit=10&page=2', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                "status",
                "success" => [
                    "current_page"
                ]

            ]);
    }

    /**
     * Get Product By Id Test Case.
     *
     * @return void
     */
    public function testGetProductByIdSuccessfully()
    {
        $user = User::factory()->make();
        $this->actingAs($user, 'api');
        $this->json('POST', 'api/products/1', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                "status" => 200
            ]);
    }

    /**
     * A feature test to delete product
     *
     * @return void
     */
    public function testDeleteProductByIdSuccessfully()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        Storage::fake('avatars');
        $file = UploadedFile::fake()->image('avatar.jpg');

        $product = Products::create([
            "name" => rand(),
            "user" => $user->id,
            "price" => 250,
            "category" => 1,
            "description" => "Video-sharing platform",
            "avatar" => $file,
        ]);

        $this->json('DELETE', 'api/products/' . $product->id)
        ->assertStatus(200)
        ->assertJson([
            'status' => '200',
            'success' => 'Deleted',
        ]);

    }
}
