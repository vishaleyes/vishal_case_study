<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;


class AuthenticationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testMustEnterEmailAndPassword()
    {
        $this->json('POST', 'api/login')
            ->assertStatus(422)
            ->assertJson([
                'status' => 422,
                "error" => "The given data was invalid.",
            ]);
    }

    public function testSuccessfulLogin()
    {
        // $user = factory(User::class)->create([
        //     'email' => 'vpanchal911@gmail.com',
        //     'password' => bcrypt('111111'),
        // ]);

        $user = User::factory()->create();

        $loginData = ['email' => $user->email, 'password' => 'password'];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'success' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'created_at',
                    'updated_at',
                    'token'
                ]
            ]);

        $this->assertAuthenticated();
    }

}
