<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Models\Products::create([
            'user' => 1,
            'name' => 'Men Olive Green Solid Round Neck Pure Cotton T-shirt',
            'price' => 219,
            'category' => 1,
            'description' => 'Olive green solid T-shirt, has a round neck, short sleeves',
            'avatar' => 'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/1996781/2018/1/9/11515489592861-Roadster-Men-Olive-Green-Solid-Round-Neck-T-shirt-1551515489592784-3.jpg'
        ]);

        \App\Models\Products::create([
            'user' => 1,
            'name' => 'Urbano Fashion',
            'price' => 494,
            'category' => 1,
            'description' => 'Men Teal Green Slim Fit Tropical Printed Pure Cotton T-shirt',
            'avatar' => 'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/12377258/2020/9/11/ce1b7bcb-a65a-4eb0-a317-42ac02718f1e1599798741705UrbanoFashionPrintedMenRoundNeckDarkGreenT-Shirt1.jpg'
        ]);

        \App\Models\Products::create([
            'user' => 1,
            'name' => 'Nautica',
            'price' => 1799,
            'category' => 1,
            'description' => 'Men Black Solid Polo Collar Pure Cotton T-shirt. Black solid T-shirt, has a polo collar, and short sleeves.',
            'avatar' => 'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/10862252/2020/4/6/d0450c66-4ee1-4326-b3ff-2542f7dae6641586163092259-Nautica-Men-Tshirts-9901586163090620-1.jpg'
        ]);

        \App\Models\Products::create([
            'user' => '1',
            'name' => 'KALINI - Kurta with Palazzos & With Dupatta',
            'price' => 776,
            'category' => 2,
            'description' => 'Women Teal Yoke Design Kurta with Palazzos & With Dupatta.',
            'avatar' => 'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/16836356/2022/3/24/c60dd80e-4c0b-404d-bf6b-18b5734541391648119245965-KALINI-Women-Blue-Yoke-Design-Kurta-with-Palazzos--With-Dupa-2.jpg'
        ]);

        \App\Models\Products::create([
            'user' => '1',
            'name' => 'AHIKA - Women Black & Green Printed Straight Kurta',
            'price' => 513,
            'category' => 2,
            'description' => 'Black and green printed straight kurta, has a nitched round neck, three-quarter sleeves, straight hem, side slits.',
            'avatar' => 'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/11056154/2019/12/5/30b0017d-7e72-4d40-9633-ef78d01719741575541717470-AHIKA-Women-Black--Green-Printed-Straight-Kurta-990157554171-1.jpg'
        ]);

        \App\Models\Products::create([
            'user' => '1',
            'name' => 'Anouk - Women Pink Printed Straight Kurta',
            'price' => 629,
            'category' => 2,
            'description' => 'Pink printed straight kurta, has a V-neck, three-quarter sleeves, straight hem, side slits.',
            'avatar' => 'https://assets.myntassets.com/h_1440,q_90,w_1080/v1/assets/images/2322979/2018/2/13/11518506064945-Anouk-Women-Peach-Coloured-Printed-Straight-Kurta-6851518506064751-2.jpg'
        ]);

        
    }
}
