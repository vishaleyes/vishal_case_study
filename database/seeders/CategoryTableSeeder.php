<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Models\Categories::create([
            'category' => 'Men',
        ]);

        \App\Models\Categories::create([
            'category' => 'Women',
        ]);

        \App\Models\Categories::create([
            'category' => 'Kids',
        ]);

        \App\Models\Categories::create([
            'category' => 'Home & Living',
        ]);

        \App\Models\Categories::create([
            'category' => 'Beauty',
        ]);
    }
}
