<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PassportController;
use App\Http\Controllers\API\ProductsController;
use App\Http\Controllers\API\CartController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [PassportController::class, 'login'])->withoutMiddleware('apiAuth:api');
Route::post('register', [PassportController::class, 'register']);


Route::get('cart', [CartController::class, 'index'])->withoutMiddleware('apiAuth:api');
Route::post('cart', [CartController::class, 'create'])->withoutMiddleware('apiAuth:api');
Route::post('cart/{id}', [CartController::class, 'show']);
Route::put('cart/{id}', [CartController::class, 'edit']);
Route::delete('cart/{id}', [CartController::class, 'destroy']);


Route::group(['middleware' => 'apiAuth:api'], function(){
    Route::post('get-details', [PassportController::class, 'getDetails']);
    Route::get('products', [ProductsController::class, 'index']);
    Route::post('products', [ProductsController::class, 'create']);
    Route::post('products/{id}', [ProductsController::class, 'show']);
    Route::delete('products/{id}', [ProductsController::class, 'destroy']);
});




